Feature: attaque
  Spécifie les stratégies d'attaques en fonction des coups adverses. Si à ce coup l'ordinateur a une position
  gagnante, il faut occuper cette position.

  Scenario: Prendre la case gagnante si elle est libre
    Given l'ordi peut gagner à ce tour
    When ordi joue
    Then ordi prend la case gagnante

  Scenario: Prendre la case centrale si c'est le premier tour
    Given l'adversaire n'a pas encore joué
    When ordi joue
    Then ordi prend la case du milieu

  Scenario: Prendre une case en coin avoisinante si l'adversaire prend la case B,D,F ou H.
    Given l'adversaire prend la case B,D,F ou H
    When ordi joue
    Then ordi prend une case avoisinante
    Then ordi prend la case avoisinante à celle posée ou gagne

  Scenario: Prendre la case opposée si l'adversaire joue un coin.
    Given l'adversaire joue un coin
    When ordi joue
    Then ordi prend la case opposée

  Scenario: Prendre une case aléatoire si aucun autre scénario ne peut être appliqué
    Given les scénarios sont tous faux
    When ordi joue
    Then ordi prend une case aléatoire