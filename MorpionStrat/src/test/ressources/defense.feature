Feature: defense
  Spécifie les stratégies de défense en fonction des coups adverses. Si au coup suivant l'adversaire aurait une position
  gagnante, il faut occuper cette position.
  Si l'adversaire a commencé et qu'il n'a pas pris la case centrale, ordi prend la case centrale.
  Si l'adversaire a commencé et a pris la case centrale, ordi prend un coin.

  Scenario: Prendre la position gagnante de l'adversaire
    Given l'adversersaire aurait un coup gagnant au prochain tour
    When ordi joue
    Then ordi prend cette position

  Scenario: Prendre la case centrale si elle est libre
    Given l'adversaire joue dans une case autre que le centre
    When ordi joue
    Then ordi a pris la case centrale

  Scenario: Prendre une case en coin si la case centrale est prise
    Given l'adversaire joue au centre
    When ordi joue
    Then ordi prend aléatoirement une case en coin

  Scenario: Prendre la case B,D,F ou H si l'adversaire possède 2 coins
    Given l'adversaire possède 2 coins
    When ordi joue
    Then ordi prend la case B,D,F ou H

  Scenario: Prendre une case aléatoire si aucun autre scénario ne peut être appliqué
    Given les scénarios sont tous faux
    When ordi joue
    Then ordi prend une case aléatoire